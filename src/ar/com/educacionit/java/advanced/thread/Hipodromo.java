package ar.com.educacionit.java.advanced.thread;

public class Hipodromo {

    public static void main(String... args) throws InterruptedException {
        Meta meta = new Meta(500);
        Caballo caballo1 = new Caballo("Caballo loco", meta);
        Caballo caballo2 = new Caballo("El negrito", meta);
        Caballo caballo3 = new Caballo("Tornado", meta);
        Caballo caballo4 = new Caballo("El llanero solitario", meta);
        Caballo caballo5 = new Caballo("Don Quijote", meta);
        
        caballo1.start();
        caballo2.start();
        caballo3.start();
        caballo4.start();
        caballo5.start();

    }

}
